<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>员工管理系统</title>
</head>
<body>
<?php include ('menu.php');
//1. 链接数据库
header("content-type:text/html;charset=utf8");
$conn=mysqli_connect("localhost","root","root","deli");
mysqli_set_charset($conn,"utf8");
$id=$_GET['id'];
//2.执行sql
$sql_select = "select * from hr where id='$id'";
$stmt = mysqli_query($conn,$sql_select);
//    var_dump($stmt);
//    die();
if ($stmt>0) {
    $stu = mysqli_fetch_assoc($stmt); // 解析数据
}else{
    die("no have this id:{$_GET['id']}");
}
?>
<h3>修改员工信息</h3>
<form action="action.php?action=edit" method="post">
    <input type="hidden" name="id" value="<?php echo $stu['id'];?>">
    <table>
        <tr>
            <td>姓名</td>
            <td><input type="text" name="name" value="<?php echo $stu['name'];?>"></td>
        </tr>
        <tr>
            <td>年龄</td>
            <td><input type="text" name="age" value="<?php echo $stu['age'];?>"></td>
        </tr>
        <tr>
            <td>性别</td>
            <td>
                <input type="radio" name="sex" value="男" <?php echo ($stu['sex'] == "男")? "checked":"";?> >男
            </td>
            <td>
                <input type="radio" name="sex" value="女" <?php echo ($stu['sex'] == "女")? "checked":"";?> >女
            </td>
        </tr>
        <tr>
            <td>部门</td>
            <td><input type="text" name="dep" value="<?php echo $stu['dep']?>"></td>
        </tr>
        <tr>
            <td>班组</td>
            <td><input type="text" name="class" value="<?php echo $stu['class']?>"></td>
        </tr>
        <tr>
            <td>身份证号</td>
            <td><input type="text" name="idn" value="<?php echo $stu['idn']?>"></td>
        </tr>
        <tr>
            <td>电话</td>
            <td><input type="text" name="tel" value="<?php echo $stu['tel']?>"></td>
        </tr>
        <tr>
            <td>员工编号</td>
            <td><input type="text" name="num" value="<?php echo $stu['num']?>"></td>
        </tr>
        <tr>
            <td> </td>
            <td><input type="submit" value="修改"></td>
            <td><input type="reset" value="重置"></td>
        </tr>
    </table>
</form>
</body>
</html>