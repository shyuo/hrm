<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>员工信息管理</title>
    <script>
        function doDel(id) {
            if(confirm('确认删除?')) {
                window.location='action.php?action=del&id='+id;
            }
        }
    </script>
</head>
<body>
<?php
include ("menu.php");
?>
<h3>浏览员工信息</h3>
<table width="750" border="1" cellspacing="0">
    <tr>
        <th>ID</th>
        <th>姓名</th>
        <th>性别</th>
        <th>年龄</th>
        <th>部门</th>
        <th>班组</th>
        <th>身份证号</th>
        <th>电话</th>
        <th>员工编号</th>
        <th>操作</th>
    </tr>
    <?php
    //    1. 链接数据库
    header("content-type:text/html;charset=utf8");
    $conn=mysqli_connect("localhost","root","root","deli");
    mysqli_set_charset($conn,"utf8");
    //2.执行sql
    $sql_select = "select * from hr";
    //3.data 解析
    foreach ( $conn->query($sql_select) as $row) {
        echo "<tr>";
        echo "<th>{$row['id']} </th>";
        echo "<th>{$row['name']}</th>";
        echo "<th>{$row['sex']} </th>";
        echo "<th>{$row['age']} </th>";
        echo "<th>{$row['dep']} </th>";
        echo "<th>{$row['class']} </th>";
        echo "<th>{$row['id_n']} </th>";
        echo "<th>{$row['tel']} </th>";
        echo "<th>{$row['number']}</th>";
        echo "<td>
          <a href='edit.php?id={$row['id']}'>修改</a>
          <a href='javascript:void(0);' onclick='doDel({$row['id']})'>删除</a>
               </td>";
        echo "</tr>";
    }
    ?>
</table>
</body>
</html>